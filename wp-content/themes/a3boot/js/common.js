$(document).ready(function() {

	$('#portfolio_grid').mixItUp();

	$('.portfolio li').click(function() {
		$('.portfolio li').removeClass('active');
		$(this).addClass('active');
	});

	$(".header_menu ul a").mPageScroll2id();

	$(".title_text h1").animated("fadeInDown", "fadeOutUp");
	$(".title_text p, .section_header").animated("fadeInUp", "fadeOutDown");

	$(".left .about_item").animated("fadeInLeft", "fadeOutDown");
	$(".right .about_item").animated("fadeInRight", "fadeOutDown");

	function height() {
		$('.main_head').css('height', $(window).height());
	};
	height();
	$(window).resize(function() {
		height();
	});

	$(".toggle_mnu").click(function() {
	 	$(".sandwich").toggleClass("active");
	});

	$(".toggle_mnu").click(function(){
		$('.header_menu').fadeToggle(800);
	});

	$(".header_menu").click(function() {
		$('.header_menu').fadeOut(800)
		$(".sandwich").toggleClass("active");
	});

});
$(window).load(function() { 
	$(".loader_inner").fadeOut(); 
	$(".loader").delay(400).fadeOut("slow");
});