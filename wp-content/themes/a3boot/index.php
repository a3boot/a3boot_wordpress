<?php get_header(); ?>

<section id="about" class="about">
    <div class="section_header">
        <h2><?php
            $idObj = get_category_by_slug('about');
            $id = $idObj->term_id;
            echo get_cat_name($id); ?></h2>
        <div class="section_description-wrapp bg_white">
            <div class="section_description">
                <?php echo category_description($id); ?>
            </div>
        </div>
    </div>
    <div class="about_content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 left">
                    <h3><?php echo get_cat_name(3); ?></h3>
                    <div class="about-icon"><i class="icon-basic-display"></i></div>

                    <?php if ( have_posts() ) : query_posts('category_name=web');
                        while (have_posts()) : the_post(); ?>
                            <div class="about_item">
                                <div class="about_item-date"><?php echo get_post_meta($post->ID, 'about_year', true); ?></div>
                                <div class="about_item-description"><strong><?php the_title(); ?></strong></div>
                                <?php the_content(); ?>
                            </div>
                      <?php endwhile; endif; wp_reset_query(); ?>

                </div>
                <div class="col-md-6 col-sm-6 right">
                    <h3><?php echo get_cat_name(4); ?></h3>
                    <div class="about-icon"><i class="icon-basic-calculator"></i></div>

                    <?php if ( have_posts() ) : query_posts('category_name=erp');
                        while (have_posts()) : the_post(); ?>
                            <div class="about_item">
                                <div class="about_item-date"><?php echo get_post_meta($post->ID, 'about_year', true); ?></div>
                                <div class="about_item-description"><strong><?php the_title(); ?></strong></div>
                                <?php the_content(); ?>
                            </div>
                        <?php endwhile; endif; wp_reset_query(); ?>

                </div>
            </div>
        </div>
    </div>
</section>

<section id="portfolio" class="portfolio bg_dark">
    <div class="section_header">
        <h2><?php
            $idObj = get_category_by_slug('portfolio');
            $id = $idObj->term_id;
            echo get_cat_name($id); ?></h2>
        <div class="section_description-wrapp">
            <div class="section_description">
                <?php echo category_description($id); ?>
            </div>
        </div>
    </div>
    <div class="section_content">
        <div class="container">
            <div class="row">
                <div class="filter_div controls">
                    <ul>
                        <li class="filter" data-filter="all">Всі роботи</li>
                        <li class="filter" data-filter=".landing">Landing-pages</li>
                        <li class="filter" data-filter=".wordpress">Wordpress</li>
                    </ul>
                </div>
                <div id="portfolio_grid">

                    <?php if ( have_posts() ) : query_posts('category_name=portfolio');
                        while (have_posts()) : the_post(); ?>

                            <div class="mix col-md-3 col-sm-6 col-xs-6 portfolio_item <?php
                            $tags = wp_get_post_tags($post->ID);
                            if ($tags) {
                                foreach ($tags as $tag) {
                                    echo ' ' . $tag->name;
                                }
                            } ?>">
                                <?php the_post_thumbnail(array(292.5, 136))?>
                                <div class="item_content">
                                    <h3><?php the_title(); ?></h3>
                                    <p><?php echo get_post_meta($post->ID, 'portfolio_description', true); ?></p>
                                    <a href="<?php echo get_post_meta($post->ID, 'portfolio_link', true); ?>">Переглянути</a>
                                </div>
                            </div>

                        <? endwhile; endif; wp_reset_query(); ?>

                </div>
            </div>
        </div>
    </div>
</section>

<section id="contacts" class="contacts bg_light">
    <div class="section_header">
        <h2><?php
            $idObj = get_category_by_slug('contacts');
            $id = $idObj->term_id;
            echo get_cat_name($id); ?></h2>
        <div class="section_description-wrapp">
            <div class="section_description">
                <?php echo category_description($id); ?>
            </div>
        </div>
    </div>
    <div class="section_content">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="contacts_box">
                        <div class="contacts_icon icon-basic-geolocalize-05"></div>
                        <h3>Адреса:</h3>
                        <p><?php $options = get_option('sample_theme_options');
                            echo $options['adress']; ?></p>
                    </div>
                    <div class="contacts_box">
                        <div class="contacts_icon icon-basic-smartphone"></div>
                        <h3>Телефон:</h3>
                        <p><?php $options = get_option('sample_theme_options');
                            echo $options['phone']; ?></p>
                    </div>
                    <div class="contacts_box">
                        <div class="contacts_icon icon-basic-webpage-img-txt"></div>
                        <h3>e-mail:</h3>
                        <p><?php $options = get_option('sample_theme_options');
                            echo $options['e-mail']; ?></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <form class="contacts_form" action="https://formspree.io/n-ort@ex.ua" method="POST">
                        <label class="form-group">
                            <span class="color_element">*</span> Ваше ім'я:
                            <input type="text" name="name" placeholder="ім'я" required>
                        </label>
                        <label class="form-group">
                            <span class="color_element">*</span> Ваш e-mail:
                            <input type="email" name="_replyto" placeholder="e-mail" required>
                        </label>
                        <label class="form-group">
                            <span class="color_element">*</span> Введіть повідомлення:
                            <textarea name="message" placeholder="Ваше повідомлення..." required></textarea>
                        </label>
                        <button>Відправити</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>