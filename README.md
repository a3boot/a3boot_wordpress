1. Install Wordpress
2. Open Mysql "databaseName(using in installation Wordpress)" table "wp_options"
3. Change "sireurl" and "home" for "http://xxx.xxx.x.xx/site_folder_name" where xxx.xxx.x.xx - ip
4. Go to the directory wp-content / themes
5. Create a folder whose name = domain name (eg "a3boot.com.ua")
6. Extract the files to the project folder created earlier
7. Extract the directory "uploads" to the 'wp-content/'
8. Export database then find and replace 'a3boot.new' to your domain name (eg "a3boot.com.ua").