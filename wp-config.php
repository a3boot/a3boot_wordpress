<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'atboot');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'n #z#>wcgbfleeXzrHBJDi!kKaAQ^I/K;b,^vFWUPJ2x;pp<BjRDQu(jT^z7`:d@');
define('SECURE_AUTH_KEY',  '^G[{0x+tEvG!ObGP?1}29UV~qZ_[ BYp [eFuj66oKw.|4d4~T>E#6FVc(R%QH5F');
define('LOGGED_IN_KEY',    'aP~;2XJw,K^y|I<JBT8EUnFiz-^)2Sz2d$>ac}>T^@reEN+a/y%DM{X5?-ot(;p.');
define('NONCE_KEY',        '|~JDPP[&y%`k.[=S*dZv27tK; xTnNL=8<DG]l&q-kuwi!rPSTj}#}$29sDRH1uT');
define('AUTH_SALT',        '`i-1Mco)nBfTi)A/m6+:H31u]Bz`61OUk^G0hw6`@6Z9`}djeKM[s!lvBsL_+>8`');
define('SECURE_AUTH_SALT', '}-eEa1Rv4UCBiS/u#{3TzkXv^2o=~ _]t0d:M,yxw},KLAeIc-Ms]i4&X:$3 .7o');
define('LOGGED_IN_SALT',   '4{RR/|VEa? mt+3):m]{OC4sO6P]tbHf3ox4PbGs;: CVEn9BK.}$}u:`4FIW_EM');
define('NONCE_SALT',       'h:G50I{I/T9/aeaZ*z.S!R7E{b4Tf6(878ePL7{CJn?uDp7Vwq8ef=^]+rAyEM> ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
